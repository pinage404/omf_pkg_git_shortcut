# [GitKraken](http://www.gitkraken.com/)
function gitkraken_auto_bg --description 'launch GitKraken on current folder'
    command gitkraken --path (realpath .) $argv >/dev/null ^ /dev/null &
end
abbr --add --global k gitkraken_auto_bg
abbr --add --global kraken gitkraken_auto_bg
abbr --add --global gkraken gitkraken_auto_bg
