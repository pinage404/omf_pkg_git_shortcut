# [Gource](http://gource.io)
function _gource_base --description 'Start gource with custom parameters'
    command gource \
        --output-framerate 60 \
        --date-format "%F %T" \
        --bloom-intensity 0.5 \
        --bloom-multiplier 0.5 \
        --elasticity 0.001 \
        $argv
end

abbr --add --global gource_anon _gource_base \
    --hide usernames

function gource_custom --description 'Start gource with custom parameters + authors list'
    _gource_base \
        --title (basename (pwd))" By "(git log --format='%aN' | sort -u | paste -sd ',' - | sed 's/,/, /g') \
        --highlight-dirs \
        --key \
        $argv
end

abbr --add --global gource_fullscreen gource_custom \
    --fullscreen
