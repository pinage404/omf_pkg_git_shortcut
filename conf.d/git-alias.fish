# git
abbr --add --global g git

set --local SUB_COMMANDS \
    bisect \
    branch \
    clone \
    commit \
    difftool \
    fetch \
    merge \
    merge \
    mergetool \
    pull \
    push \
    rebase \
    remote \
    remote \
    restore \
    show \
    status \
    switch \
    tag \
    && true

for SUB_COMMAND in $SUB_COMMANDS
    abbr --add --global $SUB_COMMAND git $SUB_COMMAND
end
