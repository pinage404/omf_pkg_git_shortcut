<img src="https://cdn.rawgit.com/oh-my-fish/oh-my-fish/e4f1c2e0219a17e2c748b824004c8d0b38055c16/docs/logo.svg" align="left" width="144px" height="144px"/>

# omf_pkg_git_shortcut

> A plugin for [Oh My Fish][omf] who provides several shortcuts for git related command

[![Fish Shell Version][fish_version]][fish]
[![Oh My Fish Framework][omf_image]][omf]
[![Licence MIT][licence_mit_image]][mit]

<br/>

- [omf_pkg_git_shortcut](#omf_pkg_git_shortcut)
    - [Install](#install)
    - [Usage](#usage)
        - [git](#git)
        - [GitKraken](#gitkraken)
        - [Gource](#gource)
    - [License](#license)


## Instal

### Fisher

```fish
fisher install gitlab.com/pinage404/omf_pkg_git_shortcut
```

I recommend to use in completion of this plugin:

```fish
fisher install gitlab.com/pinage404/omf_pkg_enlarge_your_git_alias
```

### Oh My Fish

```fish
omf install https://gitlab.com/pinage404/omf_pkg_git_shortcut
```

I recommend to use in completion of this plugin:

```fish
omf install enlarge_your_git_alias
```


## Usage

There are multiple abbreviations :


### git

```fish
# git
$ g

# git merge
$ merge

# git mergetool
$ mergetool

# git remote
$ remote

# git push
$ push

# git pull
$ pull
```


### GitKraken

Alias for [GitKraken][git-kraken-link]

```fish
$ k
$ gk
$ kraken
$ gitkraken
```


### Gource

Alias for [Gource][gource]

```fish
$ gource_anon
$ gource_custom
$ gource_fullscreen
```


## License

[MIT][mit] © [pinage404][author] et [al][contributors]



[fish]:              https://fishshell.com
[fish_version]:      https://img.shields.io/badge/fish-v2.2.0-007EC7.svg?style=flat-square

[omf]:               https://www.github.com/oh-my-fish/oh-my-fish
[omf_image]:         https://img.shields.io/badge/Oh%20My%20Fish-Framework-007EC7.svg?style=flat-square

[mit]:               ./LICENSE.md
[licence_mit_image]: https://img.shields.io/badge/license-MIT-007EC7.svg?style=flat-square

[git-kraken-link]:    http://www.gitkraken.com/

[gource]:            http://gource.io

[author]:            https://gitlab.com/pinage404
[contributors]:      https://gitlab.com/pinage404/omf_pkg_git_shortcut/graphs/master
